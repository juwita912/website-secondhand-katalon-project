package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class addProductStep {
	@Given("User on add product page")
	def navigateToAddProductPage() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
		WebUI.click(findTestObject('Object Repository/Page_Add Product/Page_add product spy/btn_masuk'))
		WebUI.setText(findTestObject('Object Repository/Page_Add Product/Page_add product spy/input_Email'), 'imanjuwita@gmail.com')
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Add Product/Page_add product spy/input_userpassword'), 'OGLiSTAGMFx0LAJ/xjX7tg==')
		WebUI.click(findTestObject('Object Repository/Page_Add Product/Page_add product spy/input_Password_commit'))
		WebUI.click(findTestObject('Object Repository/Page_Add Product/Page_add product spy/btn_Jual'))
	}
	@When("User input Nama Produk, Harga Produk, Kategori, Deskripsi and Image")
	def inputForms() {

		def testDataProduct1 = [
			[('Nama_Product') : 'hape', ('Harga_Product') : '50000', ('Kategori') : '4', ('Deskripsi') : 'tes1'],
			[('Nama_Product') : '!@#$%^', ('Harga_Product') : '50000', ('Kategori') : '4', ('Deskripsi') : 'tes1'],
			[('Nama_Product') : 'hape', ('Harga_Product') : '50000', ('Kategori') : '4', ('Deskripsi') : 'tes3'],
			[('Nama_Product') : 'hape', ('Harga_Product') : '', ('Kategori') : '4', ('Deskripsi') : 'tes4'],
			[('Nama_Product') : 'hape', ('Harga_Product') : '0', ('Kategori') : '4', ('Deskripsi') : 'tes5'],
			[('Nama_Product') : 'hape', ('Harga_Product') : '50000', ('Kategori') : '', ('Deskripsi') : 'tes6'],
			[('Nama_Product') : '12345', ('Harga_Product') : '50000', ('Kategori') : '4', ('Deskripsi') : 'tes7'],
			[('Nama_Product') : 'hape', ('Harga_Product') : '1', ('Kategori') : '4', ('Deskripsi') : 'tes8'],
			[('Nama_Product') : 'hape', ('Harga_Product') : '-1000', ('Kategori') : '4', ('Deskripsi') : 'tes9']
		]

		for (def data : testDataProduct1) {
			def nama_product = data['Nama_Product']
			def harga_product = data['Harga_Product']
			def kategori = data['Kategori']
			def deskripsi = data['Deskripsi']

			WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
			WebUI.click(findTestObject('Object Repository/Page_Add Product/Page_add product spy/btn_Jual'))
			WebUI.setText(findTestObject('Object Repository/Page_Add Product/Page_add product spy/input_productname'), nama_product )
			WebUI.setText(findTestObject('Object Repository/Page_Add Product/Page_add product spy/input_productprice'), harga_product )
			WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Add Product/Page_add product spy/select_Pilih KategoriHobiKendaraanBajuElekt_20972a'),
					'4', true)

			WebUI.setText(findTestObject('Object Repository/Page_Add Product/Page_add product spy/textarea_Deskripsi_productdescription'), deskripsi)
			//		WebUI.click(findTestObject('Object Repository/Page_Add Product/Page_add product spy/form-image'))
			//		CustomKeywords.'file_upload.FileUpload.uploadFile'(findTestObject('Object Repository/Page_Add Product/Page_add product spy/form-image'), image)
			WebUI.click(findTestObject('Object Repository/Page_Add Product/Page_add product spy/label_Terbitkan'))
		}
	}

	@Then("User on product page")
	def verifyProductPage() {

		WebUI.verifyElementVisible(findTestObject('Page_Add Product/Page_add product spy/btn_Edit'))

		WebUI.closeBrowser()
	}
}