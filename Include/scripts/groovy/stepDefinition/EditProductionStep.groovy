package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class EditProductionStep {
	@Given("User Edit Product In Form Edit Product")
	def navigateToEditPage() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
		WebUI.click(findTestObject('Page_User Login/btn_untuk masukkan login'))
		WebUI.setText(findTestObject('Page_User Login/textfield_username'), 'overlord.njoo@gmail.com')
		WebUI.setText(findTestObject('Page_User Login/textfield_password'), '31107010')
		WebUI.click(findTestObject('Page_User Login/btn_Masuk'))
	}
	@When("User update product and click product")
	def updateproduct() {

		def testDataProduct = [
			[('Nama_Product') : '!@#', ('Harga_Product') : '100000', ('Keterangan') : 'test1', ('Kategori') : '1'],
			[('Nama_Product') : 'Testing Nama 1', ('Harga_Product') : '0', ('Keterangan') : 'test2', ('Kategori') : '2'],
			[('Nama_Product') : '   12345', ('Harga_Product') : '1000000', ('Keterangan') : 'test3', ('Kategori') : '3'],
			[('Nama_Product') : '   testing awalan spasi nama barang', ('Harga_Product') : '1000', ('Keterangan') : 'test3', ('Kategori') : '4'],
			[('Nama_Product') : '    !@#', ('Harga_Product') : '100', ('Keterangan') : 'test4', ('Kategori') : '2'],
			[('Nama_Product') : 'testing barang harga minus', ('Harga_Product') : '-100000', ('Keterangan') : 'test5', ('Kategori') : '1'],
			[('Nama_Product') : '', ('Harga_Product') : '1000', ('Keterangan') : 'test6', ('Kategori') : '2'],
			[('Nama_Product') : 'testing barang harga kosong', ('Harga_Product') : '', ('Keterangan') : 'test7', ('Kategori') : '3'],
			[('Nama_Product') : 'testing barang keterangan kosong', ('Harga_Product') : '1000', ('Keterangan') : '', ('Kategori') : '4'],
			[('Nama_Product') : 'testing barang gherkins ok', ('Harga_Product') : '100000', ('Keterangan') : 'test9', ('Kategori') : '1']
		]

		for (def data : testDataProduct) {
			def nama_product = data['Nama_Product']
			def harga_product = data['Harga_Product']
			def keterangan = data ['Keterangan']
			def kategori = data ['Kategori']

			WebUI.click(findTestObject('Page_Edit_Product/btn_list_barang'))
			WebUI.click(findTestObject('Page_Edit_Product/div_testing      Hobi      Rp 100'))
			WebUI.click(findTestObject('Page_Edit_Product/btn_Edit'))
			WebUI.setText(findTestObject('Page_Edit_Product/textfield_nama_produk'), nama_product)
			WebUI.setText(findTestObject('Page_Edit_Product/textfield_harga_produk'), harga_product)
			WebUI.selectOptionByValue(findTestObject('Page_Edit_Product/select_Pilih KategoriHobiKendaraanBajuElekt_20972a'), kategori, true)
			WebUI.setText(findTestObject('Page_Edit_Product/textfield_deskripsi'), keterangan)
			WebUI.click(findTestObject('Page_Edit_Product/btn_Terbitkan'))
		}
	}
	//	@And("Click on update")
	//	def clickupdate( ) {
	//		WebUI.click(findTestObject('Page_Edit_Product/btn_Terbitkan'))
	//	}
	@Then("User on halaman edit")
	def verifyeditpage() {
		WebUI.verifyElementClickable(findTestObject('Page_Edit_Product/btn_Edit'))
		WebUI.closeBrowser()
	}
}