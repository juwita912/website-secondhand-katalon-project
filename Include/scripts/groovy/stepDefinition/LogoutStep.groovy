package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class LogoutStep {
	@Given("User login page")
	def navigateToLoginPage() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
		WebUI.click(findTestObject('Page_User Login/btn_untuk masukkan login'))
	}
	@When("User enter username and password and click login")
	def entercredentials() {
		WebUI.setText(findTestObject('Page_User Login/textfield_username'), 'overlord.njoo@gmail.com')
		WebUI.setText(findTestObject('Page_User Login/textfield_password'), '31107010')
		WebUI.click(findTestObject('Page_User Login/btn_Masuk'))
	}
	@And("Click on logout button")
	def clicklogout( ) {
		WebUI.click(findTestObject('Page_Log Out/icon person'))
		WebUI.click(findTestObject('Page_Log Out/btn_Keluar'))
	}
	@Then("User on homepage")
	def verifyhomepage() {
		WebUI.verifyElementClickable(findTestObject('Page_User Login/btn_untuk masukkan login'))
		WebUI.closeBrowser()
	}
}