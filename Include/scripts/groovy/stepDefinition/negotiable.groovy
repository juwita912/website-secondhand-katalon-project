package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class negotiable {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("user negotiate goods")
	def user_negotiate() {
		WebUI.openBrowser(GlobalVariable.url)
	}

	@When("user enter bid price and click send")
	def enter_bid_price() {
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/span_Masuk_navbar-toggler-icon'))
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/a_Masuk'))
		WebUI.setText(findTestObject('Page User Tertarik dan Ingin Nego/input_Email_useremail'), 'yogadimassetyo1@gmail.com')
		WebUI.setText(findTestObject('Page User Tertarik dan Ingin Nego/input_Password_userpassword'), 'yoga1234')
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/input_Password_commit'))
		WebUI.scrollToElement(findTestObject('Page User Tertarik dan Ingin Nego/h5_Telusuri Kategori'), 0)
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/a_Kesehatan'))
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/div_masker      Kesehatan      Rp 1.232'))
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/button_Saya tertarik dan ingin nego'))
		WebUI.setText(findTestObject('Page User Tertarik dan Ingin Nego/input_Harga Tawar_offerprice'), '6000')
	}

	@Then("offer send")
	def request_send() {
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/input_Harga Tawar_commit'))
		WebUI.closeBrowser()
	}
	
	@Given("users search for items")
	def search_item() {
		WebUI.openBrowser(GlobalVariable.url)
	}
	
	@When("user selects items")
	def select_item() {
		WebUI.scrollToElement(findTestObject('Page User Tertarik dan Ingin Nego/h5_Telusuri Kategori'), 0)
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/a_Kesehatan'))
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/div_masker      Kesehatan      Rp 1.232'))
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/button_Saya tertarik dan ingin nego'))
		WebUI.setText(findTestObject('Page User Tertarik dan Ingin Nego/input_Harga Tawar_offerprice'), '1000000')
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/input_Harga Tawar_commit'))
	}
	
	@Then("user must log in")
	def must_login() {
		WebUI.check(findTestObject('Page User Tertarik dan Ingin Nego/div_You need to sign in or sign up before continuing'))
		WebUI.closeBrowser()
	}
	
	@Given("the user wants to bid on the desired item")
	def bid_desired_item() {
		WebUI.openBrowser(GlobalVariable.url)
	}
	
	@When("user is interested in one item")
	def interested_item() {
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/span_Masuk_navbar-toggler-icon'))
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/a_Masuk'))
		WebUI.setText(findTestObject('Page User Tertarik dan Ingin Nego/input_Email_useremail'), 'yogadimassetyo1@gmail.com')
		WebUI.setText(findTestObject('Page User Tertarik dan Ingin Nego/input_Password_userpassword'), 'yoga1234')
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/input_Password_commit'))
		WebUI.scrollToElement(findTestObject('Page User Tertarik dan Ingin Nego/h5_Telusuri Kategori'), 0)
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/a_Kesehatan'))
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/div_masker      Kesehatan      Rp 1.232'))
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/button_Saya tertarik dan ingin nego'))
		WebUI.setText(findTestObject('Page User Tertarik dan Ingin Nego/input_Harga Tawar_offerprice'), '-20000')
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/input_Harga Tawar_commit'))
	}
	
	@Then("Waiting for the seller's answer")
	def waiting_seller() {
		WebUI.closeBrowser()
	}
	
	@Given("users are interested in the items being sold")
	def item_being_sold() {
		WebUI.openBrowser(GlobalVariable.url)
	}
	
	@When("user enters email")
	def enter_email() {
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/span_Masuk_navbar-toggler-icon'))
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/a_Masuk'))
		WebUI.setText(findTestObject('Page User Tertarik dan Ingin Nego/input_Email_useremail'), 'aaa@gmail.com')
		WebUI.setText(findTestObject('Page User Tertarik dan Ingin Nego/input_Password_userpassword'), 'qsdws')
		WebUI.click(findTestObject('Page User Tertarik dan Ingin Nego/input_Password_commit'))
	}
	
	@Then("invalid user email")
	def invalid_email() {
		WebUI.check(findTestObject('Page User Tertarik dan Ingin Nego/div_Invalid Email or password'))
		WebUI.closeBrowser()
	}
	
}

