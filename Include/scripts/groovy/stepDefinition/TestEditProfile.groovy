package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class EditProfile{

	@Given("User want to edit profile on profile page")
	def navigateToProfilePage() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/a_Masuk'))
		WebUI.setText(findTestObject('Object Repository/Page_User Login/textfield_username'), 'lizaapriliyani47@gmail.com')
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_User Login/textfield_password'), 'KppjyMcmxvM=')
		WebUI.click(findTestObject('Page_Edit_Profile/input_Password_commit'))
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/icon_akun'))
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/tab_profile'))
	}

	@When("User update field profile page")
	def updateprofile() {
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/input_name'), 'Junkyu KIM' )
		WebUI.selectOptionByLabel(findTestObject('Object Repository/Page_Edit_Profile/select_kota'),
				'Jakarta', true)
		WebUI.scrollToElement(findTestObject('Object Repository/Page_Edit_Profile/label_Alamat'), 3)
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/textarea_alamat'), 'Seoul')
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/input_NoHandphone'), '0878888888' )
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/btn_simpan'))
	}
	@Then("User on homepage page")
	def verifyprofile() {
		WebUI.verifyElementClickable(findTestObject('Object Repository/Page_Edit_Profile/navbar_binar'))
		WebUI.closeBrowser()
	}

	@Given("User want to update name field to symbol on profile page")
	def navigateToProfile() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/a_Masuk'))
		WebUI.setText(findTestObject('Object Repository/Page_User Login/textfield_username'), 'lizaapriliyani47@gmail.com')
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_User Login/textfield_password'), 'KppjyMcmxvM=')
		WebUI.click(findTestObject('Page_Edit_Profile/input_Password_commit'))
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/icon_akun'))
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/tab_profile'))
	}

	@When("User update name field on profile page")
	def updatenametosymbol() {
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/input_name'), '!@#!@' )
		WebUI.selectOptionByLabel(findTestObject('Object Repository/Page_Edit_Profile/select_kota'),
				'Jakarta', true)
		WebUI.scrollToElement(findTestObject('Object Repository/Page_Edit_Profile/label_Alamat'), 3)
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/textarea_alamat'), 'Seoul')
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/input_NoHandphone'), '0878888888' )
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/btn_simpan'))
	}
	@Then("User on homepage page 1")
	def verifyprofiletohomepage() {
		WebUI.verifyElementClickable(findTestObject('Object Repository/Page_Edit_Profile/navbar_binar'))
		WebUI.closeBrowser()
	}

	@Given("User want to update phone number field to letter on profile page")
	def navigateToProfilestep3() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/a_Masuk'))
		WebUI.setText(findTestObject('Object Repository/Page_User Login/textfield_username'), 'lizaapriliyani47@gmail.com')
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_User Login/textfield_password'), 'KppjyMcmxvM=')
		WebUI.click(findTestObject('Page_Edit_Profile/input_Password_commit'))
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/icon_akun'))
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/tab_profile'))
	}

	@When("User update phone number field to leter on profile page")
	def phonenumbertoletter() {
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/input_name'), '!@#!@' )
		WebUI.selectOptionByLabel(findTestObject('Object Repository/Page_Edit_Profile/select_kota'),
				'Jakarta', true)
		WebUI.scrollToElement(findTestObject('Object Repository/Page_Edit_Profile/label_Alamat'), 3)
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/textarea_alamat'), 'Seoul')
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/input_NoHandphone'), 'abcds' )
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/btn_simpan'))
	}
	@Then("User on homepage page 2")
	def verifyprofiletohomepage3() {
		WebUI.verifyElementClickable(findTestObject('Object Repository/Page_Edit_Profile/navbar_binar'))
		WebUI.closeBrowser()
	}

	@Given("User want to update name field to a number on profile page")
	def navigateToProfilestep4() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/a_Masuk'))
		WebUI.setText(findTestObject('Object Repository/Page_User Login/textfield_username'), 'lizaapriliyani47@gmail.com')
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_User Login/textfield_password'), 'KppjyMcmxvM=')
		WebUI.click(findTestObject('Page_Edit_Profile/input_Password_commit'))
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/icon_akun'))
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/tab_profile'))
	}

	@When("User update name field to number on profile page")
	def nametonumber() {
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/input_name'), '1234' )
		WebUI.selectOptionByLabel(findTestObject('Object Repository/Page_Edit_Profile/select_kota'),
				'Jakarta', true)
		WebUI.scrollToElement(findTestObject('Object Repository/Page_Edit_Profile/label_Alamat'), 3)
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/textarea_alamat'), 'Seoul')
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/input_NoHandphone'), 'abcds' )
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/btn_simpan'))
	}
	@Then("User on homepage page 3")
	def verifyprofiletohomepage4() {
		WebUI.verifyElementClickable(findTestObject('Object Repository/Page_Edit_Profile/navbar_binar'))
		WebUI.closeBrowser()
	}

	@Given("User want to update name field to a space then number on profile page")
	def navigateToProfilestep5() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/a_Masuk'))
		WebUI.setText(findTestObject('Object Repository/Page_User Login/textfield_username'), 'lizaapriliyani47@gmail.com')
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_User Login/textfield_password'), 'KppjyMcmxvM=')
		WebUI.click(findTestObject('Page_Edit_Profile/input_Password_commit'))
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/icon_akun'))
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/tab_profile'))
	}

	@When("User update name field to space then number on profile page")
	def nametospacethennumber() {
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/input_name'), ' 1234' )
		WebUI.selectOptionByLabel(findTestObject('Object Repository/Page_Edit_Profile/select_kota'),
				'Jakarta', true)
		WebUI.scrollToElement(findTestObject('Object Repository/Page_Edit_Profile/label_Alamat'), 3)
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/textarea_alamat'), 'Seoul')
		WebUI.setText(findTestObject('Object Repository/Page_Edit_Profile/input_NoHandphone'), 'abcds' )
		WebUI.click(findTestObject('Object Repository/Page_Edit_Profile/btn_simpan'))
	}
	@Then("User on homepage page 4")
	def verifyprofiletohomepage5() {
		WebUI.verifyElementClickable(findTestObject('Object Repository/Page_Edit_Profile/navbar_binar'))
		WebUI.closeBrowser()
	}
}