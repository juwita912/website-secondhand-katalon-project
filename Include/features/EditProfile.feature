Feature: Edit Profile feature

  Scenario: User want to edit profile
    Given User want to edit profile on profile page
    When User update field profile page
    Then User on homepage page
    
  Scenario: user can update profile with name changed to symbol
	  Given User want to update name field to symbol on profile page
	  When User update name field on profile page
	  Then User on homepage page 1
	  
	 Scenario: User can update the profile with the phone number changed to a letter
	  Given User want to update phone number field to letter on profile page
	  When User update phone number field to leter on profile page
	  Then User on homepage page 2
	  
	Scenario: User can update the profile with the name changed to a number.
	  Given User want to update name field to a number on profile page
	  When User update name field to number on profile page
	  Then User on homepage page 3
	  
	Scenario: The user can update the profile with the name changed to space prefix then to number
	  Given User want to update name field to a space then number on profile page
	  When User update name field to space then number on profile page
	  Then User on homepage page 4
	  
	  