$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/USER/Katalon Studio/Website Binar/Include/features/Register.feature");
formatter.feature({
  "name": "Register Feature",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to register new",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User open browser",
  "keyword": "Given "
});
formatter.match({
  "location": "RegisterStep.openbrowser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enter username,password, and email",
  "keyword": "When "
});
formatter.match({
  "location": "RegisterStep.enterdataregister()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User is success register forward to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "RegisterStep.verifyhomepage()"
});
formatter.result({
  "status": "passed"
});
});