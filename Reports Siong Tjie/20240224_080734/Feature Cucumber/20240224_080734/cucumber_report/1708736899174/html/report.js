$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/USER/Katalon Studio/Website Binar/Include/features/EditProduct.feature");
formatter.feature({
  "name": "Edit Product Feature",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to edit product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User Edit Product In Form Edit Product",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProductionStep.navigateToEditPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User update product and click product",
  "keyword": "When "
});
formatter.match({
  "location": "EditProductionStep.updateproduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on halaman edit",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProductionStep.verifyeditpage()"
});
formatter.result({
  "status": "passed"
});
});