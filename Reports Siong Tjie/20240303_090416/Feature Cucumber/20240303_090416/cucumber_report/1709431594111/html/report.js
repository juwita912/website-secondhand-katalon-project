$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/USER/Katalon Studio/Website Binar/Include/features/AddProduct.feature");
formatter.feature({
  "name": "I want to add a product in Second Hand web",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to add a product by completing all the forms",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User on add product page",
  "keyword": "Given "
});
formatter.match({
  "location": "addProductStep.navigateToAddProductPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Nama Produk, Harga Produk, Kategori, Deskripsi and Image",
  "keyword": "When "
});
formatter.match({
  "location": "addProductStep.inputForms()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on product page",
  "keyword": "Then "
});
formatter.match({
  "location": "addProductStep.verifyProductPage()"
});
formatter.result({
  "status": "passed"
});
});