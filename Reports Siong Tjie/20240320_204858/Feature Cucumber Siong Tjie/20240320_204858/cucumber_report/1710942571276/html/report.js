$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Binar Academy/Platinum Challenge/platinum_challenge_web_qae17/Include/features/Login.feature");
formatter.feature({
  "name": "Login Feature",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to login using correct credential",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to login page",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginStep.navigateToLoginPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enter username and password",
  "keyword": "When "
});
formatter.match({
  "location": "LoginStep.entercredentials()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User is navigated on homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStep.verifyhomepage()"
});
formatter.result({
  "status": "passed"
});
});