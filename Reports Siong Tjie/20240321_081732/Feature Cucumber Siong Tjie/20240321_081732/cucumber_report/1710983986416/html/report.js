$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Binar Academy/Platinum Challenge/platinum_challenge_web_qae17/Include/features/LogOut.feature");
formatter.feature({
  "name": "LogOut Feature",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to logout",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User login page",
  "keyword": "Given "
});
formatter.match({
  "location": "LogoutStep.navigateToLoginPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enter username and password and click login",
  "keyword": "When "
});
formatter.match({
  "location": "LogoutStep.entercredentials()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on logout button",
  "keyword": "And "
});
formatter.match({
  "location": "LogoutStep.clicklogout()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "LogoutStep.verifyhomepage()"
});
formatter.result({
  "status": "passed"
});
});