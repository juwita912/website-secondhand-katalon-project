<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_hapeRp 1.000.000Ditawar Rp -100</name>
   <tag></tag>
   <elementGuidId>23e0efcb-8e68-4730-ab48-2c3f56bbb4ab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.p-5.notification-list > li:nth-of-type(7) > a.notification.my-4.px-2.position-relative > div.notification-body.me-4 > div.notification-body.fs-5</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'hapeRp 1.000.000Ditawar Rp -100' or . = 'hapeRp 1.000.000Ditawar Rp -100')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[8]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ed908a1a-6eec-4e60-9512-d9d7fa1fbf66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification-body fs-5</value>
      <webElementGuid>95ec7dba-9f33-44b8-a8a2-6ca08d9dbda5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>hapeRp 1.000.000Ditawar Rp -100</value>
      <webElementGuid>6a90f13d-d2ae-43ba-acd6-3c6ad43edb5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;p-5 notification-list&quot;]/li[7]/a[@class=&quot;notification my-4 px-2 position-relative&quot;]/div[@class=&quot;notification-body me-4&quot;]/div[@class=&quot;notification-body fs-5&quot;]</value>
      <webElementGuid>2d1d953e-c4f4-450e-a318-91d733e39026</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[8]/following::div[1]</value>
      <webElementGuid>3d56c26f-eec3-45f8-b932-d54eff33af41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New alerts'])[15]/following::div[3]</value>
      <webElementGuid>df7eb8f3-9d2c-4b7b-a4b4-7fae2eb7fe57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New alerts'])[16]/preceding::div[2]</value>
      <webElementGuid>014f31e3-910a-4c58-89f6-eb7a9ed5e0b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil di terbitkan'])[7]/preceding::div[2]</value>
      <webElementGuid>5ac44f31-e820-4fd3-bcf5-d2c27d9932f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li[7]/a/div/div[2]</value>
      <webElementGuid>887770b9-af43-4dc4-ac11-0d8cc7dab57b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'hapeRp 1.000.000Ditawar Rp -100' or . = 'hapeRp 1.000.000Ditawar Rp -100')]</value>
      <webElementGuid>ee27c3a7-a11f-4b3f-8c4f-5f5837c88865</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
