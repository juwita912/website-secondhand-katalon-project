<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_hapeRp 1.000.000Ditawar Rp -100</name>
   <tag></tag>
   <elementGuidId>e864df1e-bba2-4660-bddb-9f2fa2f51caa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[12]/following::div[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul.p-5.notification-list > li:nth-of-type(7) > a.notification.my-4.px-2.position-relative > div.notification-body.me-4 > div.notification-body.fs-5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>265ad41f-5b7b-44bb-b662-fd5b9e8f37be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification-body fs-5</value>
      <webElementGuid>dc95a26a-bbd3-4e2c-9b1e-ecf156fb4173</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>hapeRp 1.000.000Ditawar Rp -100</value>
      <webElementGuid>847d67c0-7e8f-422f-8fb3-1ebb357453e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;p-5 notification-list&quot;]/li[7]/a[@class=&quot;notification my-4 px-2 position-relative&quot;]/div[@class=&quot;notification-body me-4&quot;]/div[@class=&quot;notification-body fs-5&quot;]</value>
      <webElementGuid>cccb4394-d2fc-4998-8eb7-2ef9408a382d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[12]/following::div[1]</value>
      <webElementGuid>1eee4d5d-80f5-47f1-af6c-6b0dbc18fecc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='hapeRp 1.000.000Ditawar Rp 800.000'])[3]/following::div[4]</value>
      <webElementGuid>96014e6d-1afc-4b4a-acbf-705c47275a65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New alerts'])[10]/preceding::div[2]</value>
      <webElementGuid>a3def495-225e-4e43-b0cd-4b60426749cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[13]/preceding::div[2]</value>
      <webElementGuid>6e124653-b662-4dab-981e-11b47b0da9b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li[7]/a/div/div[2]</value>
      <webElementGuid>2db506ef-044f-4ae6-85dd-9b38332f1285</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'hapeRp 1.000.000Ditawar Rp -100' or . = 'hapeRp 1.000.000Ditawar Rp -100')]</value>
      <webElementGuid>796b854e-e1f9-4a1f-8dd2-1ec4ef746a10</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
