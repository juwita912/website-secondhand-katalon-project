<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Verify_Daftar Produkmu yang Ditawar</name>
   <tag></tag>
   <elementGuidId>4a0dbcf5-5d22-4709-8159-41da72738b42</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h5.mt-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Jogja'])[1]/following::h5[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>31eb389b-d470-414e-99f3-6e5bf54fb510</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mt-5</value>
      <webElementGuid>9af3fac9-c0e3-426b-b201-f88c4f478bd3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Daftar Produkmu yang Ditawar</value>
      <webElementGuid>fa9eaeff-0231-4b3f-b440-a4e9a3c18ed9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/h5[@class=&quot;mt-5&quot;]</value>
      <webElementGuid>23e84fb8-e392-4e47-8fd0-4a6d4cda77cf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jogja'])[1]/following::h5[1]</value>
      <webElementGuid>cfa53c07-1800-42d3-9daf-a4d735bb29e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='buyerrr'])[1]/following::h5[1]</value>
      <webElementGuid>52325323-e86d-48f2-982a-336fafcfdad4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk diterima'])[1]/preceding::h5[1]</value>
      <webElementGuid>6fa528a3-888c-4aa3-888f-efe9906df16d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='hape'])[1]/preceding::h5[1]</value>
      <webElementGuid>917561ad-8b69-462e-9f83-61e739d76387</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar Produkmu yang Ditawar']/parent::*</value>
      <webElementGuid>1e531d8d-abc5-4724-a0d3-dc3ac8da4660</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h5</value>
      <webElementGuid>c929c86f-3684-4631-81b6-6b17f5a0ea9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Daftar Produkmu yang Ditawar' or . = 'Daftar Produkmu yang Ditawar')]</value>
      <webElementGuid>177f2fb8-d855-45e5-b55b-ef22d0399812</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
