<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Penawaran produk ditolak</name>
   <tag></tag>
   <elementGuidId>94f02019-5cc8-428d-a17c-9fd76c928a5c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.offer-header</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar Produkmu yang Ditawar'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5dd9221d-c6ed-4eaf-8409-db1474f4e944</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>offer-header</value>
      <webElementGuid>84c0a0ff-3610-4d53-8d92-fc1748dbb950</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
              Penawaran produk ditolak
            
            
              23 Feb, 19:38
            
          </value>
      <webElementGuid>8db832f0-ba24-4073-b5d2-2c5fa5692f87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;offers mt-5&quot;]/li[@class=&quot;offer gap-4 mt-5&quot;]/div[@class=&quot;offer-content&quot;]/div[@class=&quot;offer-header&quot;]</value>
      <webElementGuid>9af3da98-085c-4d3d-b505-9da790cf8970</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar Produkmu yang Ditawar'])[1]/following::div[2]</value>
      <webElementGuid>8cd42bdc-3740-4c56-8920-14be30b780a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jogja'])[1]/following::div[2]</value>
      <webElementGuid>521e9b8e-5613-4c7b-9261-07e3bf5e6c39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hape'])[1]/preceding::div[2]</value>
      <webElementGuid>4ba499e2-b616-4be4-87b5-a21ec9f0b61f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div/div</value>
      <webElementGuid>30b6c176-c731-4155-b84f-551e145d8164</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            
              Penawaran produk ditolak
            
            
              23 Feb, 19:38
            
          ' or . = '
            
              Penawaran produk ditolak
            
            
              23 Feb, 19:38
            
          ')]</value>
      <webElementGuid>a5c8e9c7-b642-4fe2-80b8-1aa1ddef0559</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
