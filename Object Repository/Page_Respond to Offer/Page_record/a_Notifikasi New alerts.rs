<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Notifikasi New alerts</name>
   <tag></tag>
   <elementGuidId>d842cdbd-ffd4-41cd-b417-8584bc662737</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.nav-item.me-0.me-lg-2.fs-5.d-block.d-xl-none.position-relative > a.nav-link.d-flex.align-items-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>66668e30-5c54-4d2e-b725-ef2b7352e2aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link d-flex align-items-center</value>
      <webElementGuid>38879c18-88b9-4331-aad2-2a254a35384c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/notifications</value>
      <webElementGuid>d39ee1c5-d7df-4790-8148-b2ad74cbcb73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
      Notifikasi

        
          New alerts
        
</value>
      <webElementGuid>d837e284-2e98-4334-bcff-22087fb17327</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item me-0 me-lg-2 fs-5 d-block d-xl-none position-relative&quot;]/a[@class=&quot;nav-link d-flex align-items-center&quot;]</value>
      <webElementGuid>110a59a5-acf8-457a-9cef-4d498b9ea384</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[2]/a</value>
      <webElementGuid>310b0f47-81e9-4f41-bd05-d07a66703430</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Produk Saya'])[1]/following::a[1]</value>
      <webElementGuid>bc26690d-a45e-46f1-83e8-f2ddaf2e3751</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/notifications')]</value>
      <webElementGuid>2fbe25af-2690-4497-87bc-42e495431493</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a</value>
      <webElementGuid>f2239c98-bbf9-4ef5-a9fe-a25ddcf630cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/notifications' and (text() = '
      
      Notifikasi

        
          New alerts
        
' or . = '
      
      Notifikasi

        
          New alerts
        
')]</value>
      <webElementGuid>06b73a74-d4c0-4277-8fee-ac0ce10753f6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
