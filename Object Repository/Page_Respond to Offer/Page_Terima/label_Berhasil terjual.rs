<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Berhasil terjual</name>
   <tag></tag>
   <elementGuidId>acd5471c-86b4-4de4-aa43-db71466be36c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>label.form-check-label.mb-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='statusModal89830']/div/form/div[2]/div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>da65dd6d-efc2-4629-8fc2-c9f6ec74857d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-check-label mb-2</value>
      <webElementGuid>45c58871-0994-4cab-b532-e11e9df2a623</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>offer_status_finished</value>
      <webElementGuid>aeee99ce-365d-4c76-abfa-f4729566d7c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                  Berhasil terjual
</value>
      <webElementGuid>3bc9213a-5925-4016-9192-58ad04fbce17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;statusModal89830&quot;)/div[@class=&quot;modal-dialog modal-dialog-centered&quot;]/form[@class=&quot;modal-content px-4 py-2&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;form-check&quot;]/label[@class=&quot;form-check-label mb-2&quot;]</value>
      <webElementGuid>2ae9fa71-4cf2-4afc-abfc-36ad3076e24b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='statusModal89830']/div/form/div[2]/div/label</value>
      <webElementGuid>ab156427-8a50-42c6-81bc-ae20632b579f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Perbarui status penjualan produkmu'])[1]/following::label[1]</value>
      <webElementGuid>81009fdf-5cd2-4b49-bc78-d7fbb03ff246</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hubungi di'])[1]/following::label[1]</value>
      <webElementGuid>01a4268d-cdbc-4184-b304-cc4490a267dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Batalkan transaksi'])[1]/preceding::label[1]</value>
      <webElementGuid>674e8b6a-729c-4714-9095-ada93a03589c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil terjual'])[2]/preceding::label[2]</value>
      <webElementGuid>fc967e4e-569f-46b4-9a6f-7e18e8c6445d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Berhasil terjual']/parent::*</value>
      <webElementGuid>d8e18761-f5dd-447f-8fef-e1a957f08328</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label</value>
      <webElementGuid>25763b93-9f40-426c-997a-e9d08cf4f6ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = '
                  Berhasil terjual
' or . = '
                  Berhasil terjual
')]</value>
      <webElementGuid>a700beae-b03d-430d-b269-35cd646905b2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
