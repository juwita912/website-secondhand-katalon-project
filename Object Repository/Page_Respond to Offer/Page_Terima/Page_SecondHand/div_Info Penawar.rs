<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Info Penawar</name>
   <tag></tag>
   <elementGuidId>c44b2fec-e5fd-4333-b4ea-264c6de3ca70</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.container</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Status produk berhasil diperbarui'])[1]/preceding::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7b6345fd-fbb7-48b7-8499-49cc82b563a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container</value>
      <webElementGuid>14b0ac64-6bd4-48ec-8a30-4409cbeaf1c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    
      
    
      
        Info Penawar
      
  </value>
      <webElementGuid>40e567a7-e19d-4e69-8eaf-3dcfd1127366</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/nav[@class=&quot;navbar navbar-expand-lg navbar-light bg-white shadow fixed-top&quot;]/div[@class=&quot;container&quot;]</value>
      <webElementGuid>45314114-7109-4d96-90cc-3f2bcf5a2dde</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status produk berhasil diperbarui'])[1]/preceding::div[2]</value>
      <webElementGuid>e6faf922-29bd-40cf-bd58-9eb48815a1e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div</value>
      <webElementGuid>5823557d-51b3-4368-9035-843bef10acbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    
    
      
    
      
        Info Penawar
      
  ' or . = '
    
    
      
    
      
        Info Penawar
      
  ')]</value>
      <webElementGuid>bca8f45b-d313-4537-b7ba-d9342a51ad8a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
