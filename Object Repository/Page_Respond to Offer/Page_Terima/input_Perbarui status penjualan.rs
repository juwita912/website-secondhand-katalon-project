<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Perbarui status penjualan</name>
   <tag></tag>
   <elementGuidId>95bff1c2-deee-44ca-a834-76dac81da88d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#offer_status_finished</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='offer_status_finished']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>88122e8b-90b8-40f2-8860-bbfd3b225021</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-check-input me-2</value>
      <webElementGuid>bb75208f-826c-4f4f-87fb-0ce74fc15093</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>cbe04be0-4380-4dcb-b9f3-08946918704d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>finished</value>
      <webElementGuid>6aa03d99-89ba-4360-99fe-24c2dd42c57c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>offer[status]</value>
      <webElementGuid>d095389e-932e-4a6d-8dc5-9574c17bf98c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>offer_status_finished</value>
      <webElementGuid>c0460753-ada3-4b66-ac4b-dba1db844f54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;statusModal89831&quot;)/div[@class=&quot;modal-dialog modal-dialog-centered&quot;]/form[@class=&quot;modal-content px-4 py-2&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;form-check&quot;]/input[@id=&quot;offer_status_finished&quot;]</value>
      <webElementGuid>1c1856d2-a038-4e2e-a47d-566493f60f31</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='offer_status_finished']</value>
      <webElementGuid>c71629e1-01ff-49ea-8a4a-c008f6ccfd9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='statusModal89831']/div/form/div[2]/div/input</value>
      <webElementGuid>d96544ed-349b-44c5-8765-368051f67f65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input</value>
      <webElementGuid>1a72521a-9496-4277-9759-3ed1fd4e2930</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'radio' and @name = 'offer[status]' and @id = 'offer_status_finished']</value>
      <webElementGuid>b55d469d-6047-4897-b47c-4d6dfacb7d5a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
