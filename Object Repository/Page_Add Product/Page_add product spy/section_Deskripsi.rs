<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Deskripsi</name>
   <tag></tag>
   <elementGuidId>eb0f2310-60af-4d4b-9003-e12d645c2296</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Product was successfully created.'])[1]/following::section[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>section.pt-5.mt-5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
      <webElementGuid>4e30eb63-0ed0-44ed-b90b-7b0cb3413cbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pt-5 mt-5</value>
      <webElementGuid>6e3aa2ed-3e78-4a15-a243-a21fc69f3b5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
  
    
        

          
              
                
              
          

        

      
        
          Deskripsi
          1
        
      

    

    
      
        
          tes
          Elektronik
          Rp 1.233
            Edit
            Delete
        
      

      
        
          

          
            Seller J
            Jogja
          
        
      
    
  



    </value>
      <webElementGuid>745b8efc-929a-43db-83fb-5fb8c4124f88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]</value>
      <webElementGuid>9c101a68-ec5a-47a0-95cf-7e2a769e98b6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Product was successfully created.'])[1]/following::section[1]</value>
      <webElementGuid>deb54254-06df-42b7-b8fa-0dd2ec746865</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section</value>
      <webElementGuid>587f7c99-3643-446b-8b50-2525ec65682d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[(text() = '
      
  
    
        

          
              
                
              
          

        

      
        
          Deskripsi
          1
        
      

    

    
      
        
          tes
          Elektronik
          Rp 1.233
            Edit
            Delete
        
      

      
        
          

          
            Seller J
            Jogja
          
        
      
    
  



    ' or . = '
      
  
    
        

          
              
                
              
          

        

      
        
          Deskripsi
          1
        
      

    

    
      
        
          tes
          Elektronik
          Rp 1.233
            Edit
            Delete
        
      

      
        
          

          
            Seller J
            Jogja
          
        
      
    
  



    ')]</value>
      <webElementGuid>8f874476-0770-4a21-9734-24ccb60ce5ed</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
