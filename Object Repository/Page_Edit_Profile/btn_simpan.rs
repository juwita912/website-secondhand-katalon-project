<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_simpan</name>
   <tag></tag>
   <elementGuidId>196f829d-7bf5-4fd1-b56f-e85a5361b8be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;commit&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='commit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>bee46eba-0b3c-4b41-bbe2-d140d42f209a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>9c39c423-50cc-4232-afa8-72af1dff4f09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>commit</value>
      <webElementGuid>95cdd236-6b4d-49a3-9ea3-9f8af47d2f2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Simpan</value>
      <webElementGuid>2f951fba-f806-4461-8d48-e00c973f9ba3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary w-100 rounded-pill p-3</value>
      <webElementGuid>e5919419-f9cd-4cc7-b95e-cc7fb4c47201</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-disable-with</name>
      <type>Main</type>
      <value>Simpan</value>
      <webElementGuid>584c81ee-c7cc-47af-902c-402ba61e8148</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container container-sm position-relative&quot;]/section[@class=&quot;row mt-5&quot;]/div[@class=&quot;col-11&quot;]/form[1]/div[@class=&quot;mb-4 d-flex gap-5&quot;]/input[@class=&quot;btn btn-primary w-100 rounded-pill p-3&quot;]</value>
      <webElementGuid>8830c209-07ab-4ce9-9e79-d839b88d1df1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='commit']</value>
      <webElementGuid>019d8d49-0aa8-4e04-a1ed-a27971fb5a59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/input</value>
      <webElementGuid>44821dfa-22cd-4014-8720-f8e6753eafe0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'commit']</value>
      <webElementGuid>fd263b88-5985-4218-a26f-71fb0f30439d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
