<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Email</name>
   <tag></tag>
   <elementGuidId>0bee0a37-7de7-411f-87d4-2d646dc69c52</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.field.mb-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='new_user']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#new_user div >> internal:has-text=&quot;Email&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>69e905c1-ac5a-4c93-ad7b-9743c502c960</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>field mb-4</value>
      <webElementGuid>b7d059e8-826c-45ab-8fb8-b62faf32618f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Email
            
          </value>
      <webElementGuid>5a152b90-8943-4e90-9e4f-be575f9b222f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;new_user&quot;)/div[@class=&quot;field mb-4&quot;]</value>
      <webElementGuid>ba079b8a-74e0-4327-b834-a7923c734ac5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='new_user']/div</value>
      <webElementGuid>cd01cf76-d057-4093-a92d-58c2b304f974</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk'])[1]/following::div[1]</value>
      <webElementGuid>83a47e3b-0ecb-499c-9893-88d079a0631e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/following::div[3]</value>
      <webElementGuid>6cde6e6b-a790-4b18-8695-e590114ca13e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/preceding::div[1]</value>
      <webElementGuid>03a17730-c2e4-4715-9e7f-fd10b21f1e95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div</value>
      <webElementGuid>fee5365b-d955-4b7c-a207-936f2365654a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            Email
            
          ' or . = '
            Email
            
          ')]</value>
      <webElementGuid>9e8ac8ad-349e-40e1-9d6e-850002f2ddf4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
