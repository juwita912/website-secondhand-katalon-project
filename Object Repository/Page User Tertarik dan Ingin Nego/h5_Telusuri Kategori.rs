<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_Telusuri Kategori</name>
   <tag></tag>
   <elementGuidId>9e2aa9ff-3321-4a23-952b-f172d7a531a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/following::h5[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h5.fw-bold.mb-4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>c4b50f30-dca5-44e9-b642-647d85120f81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fw-bold mb-4</value>
      <webElementGuid>455409dd-86ac-46e6-9c4f-04d8a381b5bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Telusuri Kategori</value>
      <webElementGuid>ea278778-9548-4117-b7c0-830ff2046293</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container&quot;]/h5[@class=&quot;fw-bold mb-4&quot;]</value>
      <webElementGuid>4513d2d0-3955-4441-b73a-834c9a80c742</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/following::h5[2]</value>
      <webElementGuid>e95dfaff-3075-4a72-a9b4-cad57042eb2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulan RamadhanBanyak diskon!'])[1]/following::h5[2]</value>
      <webElementGuid>ef874a63-2c73-4115-8823-95e39eff6755</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Telusuri Kategori']/parent::*</value>
      <webElementGuid>1f66dcc0-f47e-4bec-8005-58a0547a65bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[2]/h5</value>
      <webElementGuid>4897cba2-e89f-4da3-b4db-484d3edcf489</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Telusuri Kategori' or . = 'Telusuri Kategori')]</value>
      <webElementGuid>55438ced-3775-496f-942a-bd17d04457a8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
