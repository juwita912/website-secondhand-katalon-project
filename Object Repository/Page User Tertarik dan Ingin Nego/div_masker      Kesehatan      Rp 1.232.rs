<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_masker      Kesehatan      Rp 1.232</name>
   <tag></tag>
   <elementGuidId>4bb787b7-4d61-4158-b7f5-33784519ee82</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='products']/div[2]/a/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=link[name=&quot;masker Kesehatan Rp 1.232&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>940f5730-d92e-4342-b652-87711c118dce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card px-0 border-0 shadow h-100 pb-4 rounded-4</value>
      <webElementGuid>99b152ab-7177-4b63-83bf-b8d80796dde5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      

    
      masker
      Kesehatan
      Rp 1.232
    
  </value>
      <webElementGuid>70016a61-c870-49cc-8ad8-3d8bca02e800</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products&quot;)/div[@class=&quot;col-12 col-md-4 col-lg-3&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]</value>
      <webElementGuid>f70512c1-2e38-494e-9152-b713a2213967</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='products']/div[2]/a/div</value>
      <webElementGuid>5bf6d5ce-e7e6-4c20-804d-5af551e80262</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 20.000'])[1]/following::div[2]</value>
      <webElementGuid>0f57445f-a55b-4fc5-80ba-79d8ece12941</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='aqua'])[1]/following::div[2]</value>
      <webElementGuid>95259bb6-10c3-4e2b-bb07-73b83dae4018</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a/div</value>
      <webElementGuid>74678f12-1e78-4479-946a-97acf19fa383</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      

    
      masker
      Kesehatan
      Rp 1.232
    
  ' or . = '
      

    
      masker
      Kesehatan
      Rp 1.232
    
  ')]</value>
      <webElementGuid>65ffe880-1aa6-4dff-9e82-4eb0684a5455</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
