<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_You need to sign in or sign up before continuing</name>
   <tag></tag>
   <elementGuidId>48aa5538-d08f-43a7-91d8-643889b96667</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/preceding::div[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-danger.alert-dismissible.position-fixed.top-0.start-50.translate-middle-x.mt-5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e2f35cd2-b690-4cf3-b20b-233cffe51019</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger alert-dismissible position-fixed top-0 start-50 translate-middle-x mt-5</value>
      <webElementGuid>a7264905-cbdc-40e6-995b-bb5d05ea8ad7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>alert</value>
      <webElementGuid>65e53039-74b8-4806-815c-832ec902f19c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        You need to sign in or sign up before continuing.
        
      </value>
      <webElementGuid>e8436651-c8b0-4349-94df-e3425ef86733</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;alert alert-danger alert-dismissible position-fixed top-0 start-50 translate-middle-x mt-5&quot;]</value>
      <webElementGuid>260061b4-2741-411d-9463-6c7543f1a333</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/preceding::div[1]</value>
      <webElementGuid>15da348e-412a-4e7d-8d9c-962e6679e3f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk'])[1]/preceding::div[2]</value>
      <webElementGuid>737569bf-85fc-46d8-901e-2d27f2c34593</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='You need to sign in or sign up before continuing.']/parent::*</value>
      <webElementGuid>643c47ff-a6e2-43cc-ba4e-715e4c50ca8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div</value>
      <webElementGuid>32c75e5d-55d8-4077-90af-0a4dc698f2ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        You need to sign in or sign up before continuing.
        
      ' or . = '
        You need to sign in or sign up before continuing.
        
      ')]</value>
      <webElementGuid>07c51887-91d0-4fbc-80f5-67ea393a3e24</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
