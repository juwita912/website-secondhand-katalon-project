<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Masuk_navbar-toggler-icon</name>
   <tag></tag>
   <elementGuidId>1e04dc97-7b96-40b8-bde9-a6bd2a11e78b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.navbar-toggler-icon</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2bf21ee7-7586-402a-89a1-b26ea43a868b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navbar-toggler-icon</value>
      <webElementGuid>b2b927e0-c941-4544-8a8f-9d946150d5e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/nav[@class=&quot;navbar navbar-expand-lg navbar-light bg-white shadow fixed-top&quot;]/div[@class=&quot;container&quot;]/button[@class=&quot;navbar-toggler&quot;]/span[@class=&quot;navbar-toggler-icon&quot;]</value>
      <webElementGuid>2f33bf20-e055-44db-a6a5-b60d27fe6c20</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span</value>
      <webElementGuid>2c1e90c6-c64f-45c6-9775-c0e317dd2b85</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
