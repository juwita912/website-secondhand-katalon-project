<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>notif_harga tidak boleh kosong</name>
   <tag></tag>
   <elementGuidId>0f64995b-960a-434f-809b-cb7ca0fd3fb7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.form-text.text-danger</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Harga Produk'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>665119cb-a562-4b42-a357-e4090cbbfd5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-text text-danger</value>
      <webElementGuid>c815d64c-632a-4b5e-aedb-e5e449efe42e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Price can't be blank
          </value>
      <webElementGuid>12d7dba5-4001-49e0-9281-e007b5f1ab3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container container-sm position-relative&quot;]/section[@class=&quot;row mt-5&quot;]/div[@class=&quot;col-11&quot;]/form[1]/div[@class=&quot;mb-4&quot;]/div[@class=&quot;form-text text-danger&quot;]</value>
      <webElementGuid>85cdb9f8-a45d-4545-a6ca-531c6a866400</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Harga Produk'])[1]/following::div[2]</value>
      <webElementGuid>22ac551d-050f-4a2c-a6e0-c6053b588f40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama Produk'])[1]/following::div[4]</value>
      <webElementGuid>149d1692-e038-4fb2-b9b3-49394a2f6763</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/preceding::div[1]</value>
      <webElementGuid>349abcc2-f715-4ad2-86e5-93933bb7bcd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/preceding::div[2]</value>
      <webElementGuid>9c8e4b1b-a53c-4da6-9383-6bc6a66877f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]</value>
      <webElementGuid>7ed38ac6-3c49-430b-a429-c2768e8fc1a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
            Price can&quot; , &quot;'&quot; , &quot;t be blank
          &quot;) or . = concat(&quot;
            Price can&quot; , &quot;'&quot; , &quot;t be blank
          &quot;))]</value>
      <webElementGuid>fc5aa440-72b1-4eeb-84a7-e9d32d239568</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
