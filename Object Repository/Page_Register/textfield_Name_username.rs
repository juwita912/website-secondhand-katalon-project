<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textfield_Name_username</name>
   <tag></tag>
   <elementGuidId>3a2d995c-f4d4-4ae4-88ad-220ba63d1120</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='user_name']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#user_name</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>5caf9044-fd3b-4448-adc5-c49401acdc5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>14b16ec8-9377-47a6-8239-0458182bfe59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control rounded-4 p-3</value>
      <webElementGuid>064a6a8a-ce70-4f17-9f06-fb491169b58d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Nama Lengkap</value>
      <webElementGuid>ea156c9e-8db9-40eb-b72f-70483d1eec8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>bc6fa88f-2558-442e-8f8d-67d0012aaed2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>user[name]</value>
      <webElementGuid>7e912e3c-515c-4f94-b8e8-0f8235d3eeb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user_name</value>
      <webElementGuid>16cf45ee-928b-469a-97f1-55127d60d0ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;user_name&quot;)</value>
      <webElementGuid>5ef3eb3a-985e-4b8c-8cf9-ad7348752ff7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='user_name']</value>
      <webElementGuid>b27afbb0-5361-4df0-bedf-2389ea80d6cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='new_user']/div/input</value>
      <webElementGuid>a85b1f41-de64-4f52-ae2e-51b5ca9043a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input</value>
      <webElementGuid>2a085df2-5f5c-440d-b2be-f89827a0995b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Nama Lengkap' and @type = 'text' and @name = 'user[name]' and @id = 'user_name']</value>
      <webElementGuid>07544fc2-79ce-43ac-bb43-06e49632a466</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
