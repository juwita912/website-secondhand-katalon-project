<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_login_commit</name>
   <tag></tag>
   <elementGuidId>b7f615b3-6ce2-45e5-9928-6b6c92e7c3af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;commit&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='commit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>53aacdc8-4f54-4b27-aef2-0652639ae97e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>bf034d12-3978-4ab2-a7ce-06c0a146aca8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>commit</value>
      <webElementGuid>5880aaa8-45c3-4bf6-86bb-706f23b03643</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Masuk</value>
      <webElementGuid>4bcad534-2cd7-4f9e-a293-c1c298c0b31b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary w-100 rounded-4 p-3 fw-bold</value>
      <webElementGuid>ee412393-ccf4-4e9f-9a9c-d3e21bdbed12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-disable-with</name>
      <type>Main</type>
      <value>Masuk</value>
      <webElementGuid>b05a41a1-67d6-4d11-9c3b-8420e812cd0e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;new_user&quot;)/div[@class=&quot;actions&quot;]/input[@class=&quot;btn btn-primary w-100 rounded-4 p-3 fw-bold&quot;]</value>
      <webElementGuid>c299a12a-362d-43f6-b80d-e4f552ec79e5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='commit']</value>
      <webElementGuid>60d8e23d-b9bf-41d1-8e63-5e542bf6b1ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='new_user']/div[3]/input</value>
      <webElementGuid>243fc243-1e9a-4b0d-b96c-1c44714251c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>0716a6c8-522f-49cf-b684-4f5170c629f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @name = 'commit']</value>
      <webElementGuid>6e21d607-db69-410e-9007-0bcb7859b310</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
