$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("D:/binar/clone juwita repo website/platinum_challenge_web_qae17/Include/features/RespondToOffer.feature");
formatter.feature({
  "name": "Respond to Offer",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to respond accept product offer with normal price",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User on notification page of product offer with normal price",
  "keyword": "Given "
});
formatter.match({
  "location": "RespondtoOffer.navigateToNotificationPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User accept product offer with normal price",
  "keyword": "When "
});
formatter.match({
  "location": "RespondtoOffer.clickAccept()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on What\u0027s App Page",
  "keyword": "Then "
});
formatter.match({
  "location": "RespondtoOffer.verifyWhatsApp()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User want to reject product offer",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User on notification page of product offer to reject",
  "keyword": "Given "
});
formatter.match({
  "location": "RespondtoOffer.navigateToNotificationRejectPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User reject product offer",
  "keyword": "When "
});
formatter.match({
  "location": "RespondtoOffer.clickReject()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on notification page",
  "keyword": "Then "
});
formatter.match({
  "location": "RespondtoOffer.verifyReject()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User want to accept product offer with negative price",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User on notification page of product offer with negative price",
  "keyword": "Given "
});
formatter.match({
  "location": "RespondtoOffer.navigateToNotificationNegative()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User accept product offer with negative price",
  "keyword": "When "
});
formatter.match({
  "location": "RespondtoOffer.clickAcceptNegative()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User notification page with negative price",
  "keyword": "Then "
});
formatter.match({
  "location": "RespondtoOffer.verifyNegative()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User want to accept product offer with higher price",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User on notification page of product offer with higher price",
  "keyword": "Given "
});
formatter.match({
  "location": "RespondtoOffer.navigateToNotificationPageHiherPrice()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User accept product offer",
  "keyword": "When "
});
formatter.match({
  "location": "RespondtoOffer.clickAcceptHigherPrice()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User notification page with higher price",
  "keyword": "Then "
});
formatter.match({
  "location": "RespondtoOffer.verifyHigherPrice()"
});
formatter.result({
  "status": "passed"
});
});