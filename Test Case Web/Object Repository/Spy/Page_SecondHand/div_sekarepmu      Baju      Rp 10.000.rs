<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_sekarepmu      Baju      Rp 10.000</name>
   <tag></tag>
   <elementGuidId>0415dfea-f227-4bb3-8e48-c69a96bba757</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='products']/div[2]/a/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3d7d1221-3fcc-4ef0-b19d-550e6c5a4603</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card px-0 border-0 shadow h-100 pb-4 rounded-4</value>
      <webElementGuid>c0382ad0-f0ca-46e5-ab58-62548a7ec8f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      

    
      sekarepmu
      Baju
      Rp 10.000
    
  </value>
      <webElementGuid>649e59a8-15b7-4c02-b1e3-4d840f18c9a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products&quot;)/div[@class=&quot;col-12 col-md-4 col-lg-3&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]</value>
      <webElementGuid>daebc79c-4620-488f-863f-b94d1162ad66</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='products']/div[2]/a/div</value>
      <webElementGuid>d1081b47-6a6b-4b4c-8bbc-24aca9c1349d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 155.555'])[1]/following::div[2]</value>
      <webElementGuid>e59def03-8d3a-4765-b1b1-74f33bf6fc3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='hapee'])[1]/following::div[2]</value>
      <webElementGuid>ab30ab86-5c5c-4dad-ad04-172515625ebc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a/div</value>
      <webElementGuid>0b6c5905-68a3-4bb7-9510-00aeb670298c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      

    
      sekarepmu
      Baju
      Rp 10.000
    
  ' or . = '
      

    
      sekarepmu
      Baju
      Rp 10.000
    
  ')]</value>
      <webElementGuid>a6828b3d-4361-488a-8e61-b701acdcc751</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
