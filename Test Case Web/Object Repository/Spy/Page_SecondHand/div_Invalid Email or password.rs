<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Invalid Email or password</name>
   <tag></tag>
   <elementGuidId>9b15b439-ae3b-481a-a89c-3bece46d45cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-danger.alert-dismissible.position-fixed.top-0.start-50.translate-middle-x.mt-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/preceding::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>fd06a757-160b-4927-8ed5-b32e521500a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger alert-dismissible position-fixed top-0 start-50 translate-middle-x mt-5</value>
      <webElementGuid>918ad828-aea3-428f-82b2-a06ee75b060c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>alert</value>
      <webElementGuid>073a8e6c-c172-4b45-a4e6-98b184382e1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Invalid Email or password.
        
      </value>
      <webElementGuid>bac71695-215d-4d19-bc54-f46068e31c87</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;alert alert-danger alert-dismissible position-fixed top-0 start-50 translate-middle-x mt-5&quot;]</value>
      <webElementGuid>9aa4ef65-8132-4b84-94c8-d3446395ca5b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/preceding::div[1]</value>
      <webElementGuid>0422c0ed-bb5e-465c-bae4-b5331fa51d47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk'])[1]/preceding::div[2]</value>
      <webElementGuid>9eb70342-9dc5-4fe6-9189-d418a1658223</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Invalid Email or password.']/parent::*</value>
      <webElementGuid>4553b6dd-a2ed-4688-b040-68d1f0b4564f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div</value>
      <webElementGuid>b795b621-1878-4abe-8fcc-66884cd15151</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Invalid Email or password.
        
      ' or . = '
        Invalid Email or password.
        
      ')]</value>
      <webElementGuid>1754c950-47b6-4690-91f3-f3972f96eba0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
