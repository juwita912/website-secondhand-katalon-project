import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.click(findTestObject('Spy/Page_SecondHand/button_Masuk_navbar-toggler'))

WebUI.click(findTestObject('Spy/Page_SecondHand/a_Masuk'))

WebUI.setText(findTestObject('Spy/Page_SecondHand/input_Email_useremail'), 'yogadimassetyo1@gmail.com')

WebUI.setText(findTestObject('Spy/Page_SecondHand/input_Password_userpassword'), 'yoga1234')

WebUI.click(findTestObject('Spy/Page_SecondHand/input_Password_commit'))

WebUI.scrollToElement(findTestObject('Spy/Page_SecondHand/h5_Telusuri Kategori'), 0)

WebUI.click(findTestObject('Spy/Page_SecondHand/div_sekarepmu      Baju      Rp 10.000'))

WebUI.click(findTestObject('Spy/Page_SecondHand/button_Saya tertarik dan ingin nego'))

WebUI.setText(findTestObject('Spy/Page_SecondHand/input_Harga Tawar_offerprice'), '6000')

WebUI.click(findTestObject('Spy/Page_SecondHand/input_Harga Tawar_commit'))

