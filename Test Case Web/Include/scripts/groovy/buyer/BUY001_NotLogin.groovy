package buyer
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class BUY001_NotLogin {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("user buy goods")
	def user_makes_an_offer() {
		WebUI.openBrowser(GlobalVariable.url)
	}

	@When("user has not login")
	def user_select_item() {
		WebUI.scrollToElement(findTestObject('Spy/Page_SecondHand/h5_Telusuri Kategori'), 0)

		WebUI.click(findTestObject('Spy/Page_SecondHand/a_Elektronik'))

		WebUI.click(findTestObject('Spy/Page_SecondHand/div_hapee      Elektronik      Rp 155.555'))

		WebUI.click(findTestObject('Spy/Page_SecondHand/button_Saya tertarik dan ingin nego'))

		WebUI.setText(findTestObject('Spy/Page_SecondHand/input_Harga Tawar_offerprice'), '1000000')

		WebUI.click(findTestObject('Spy/Page_SecondHand/input_Harga Tawar_commit'))
	}

	@Then("user need to sign in")
	def offer_cannot_be_continued() {
		WebUI.check(findTestObject('Spy/Page_SecondHand/div_You need to sign in or sign up before continuing'))
	}
}