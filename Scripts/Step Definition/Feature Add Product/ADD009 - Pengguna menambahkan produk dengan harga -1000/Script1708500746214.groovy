import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://secondhand.binaracademy.org/')

WebUI.click(findTestObject('Object Repository/Page_Add Product/Page_add product spy/btn_masuk'))

WebUI.setText(findTestObject('Object Repository/Page_Add Product/Page_add product spy/input_Email'), 'imanjuwita@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Add Product/Page_add product spy/input_userpassword'), 'OGLiSTAGMFx0LAJ/xjX7tg==')

WebUI.click(findTestObject('Object Repository/Page_Add Product/Page_add product spy/input_Password_commit'))

WebUI.click(findTestObject('Object Repository/Page_Add Product/Page_add product spy/btn_Jual'))

WebUI.setText(findTestObject('Object Repository/Page_Add Product/Page_add product spy/input_productname'), 'hape')

WebUI.setText(findTestObject('Object Repository/Page_Add Product/Page_add product spy/input_productprice'), '1000000')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Add Product/Page_add product spy/select_Pilih KategoriHobiKendaraanBajuElekt_20972a'), 
    '4', true)

WebUI.setText(findTestObject('Object Repository/Page_Add Product/Page_add product spy/textarea_Deskripsi_productdescription'), 
    'sangat murah dan bagus')

//CustomKeywords.'file_upload.FileUpload.uploadFile'(findTestObject('Object Repository/Page_Add Product/Page_add product spy/form-image'), 'C:\\Users\\User\\android.jpg')
WebUI.click(findTestObject('Object Repository/Page_Add Product/Page_add product spy/label_Terbitkan'))

WebUI.closeBrowser()

