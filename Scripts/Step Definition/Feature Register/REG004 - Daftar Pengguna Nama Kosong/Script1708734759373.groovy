import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.click(findTestObject('Page_User Login/btn_untuk masukkan login'))

WebUI.click(findTestObject('Page_Register/button_Daftar di sini'))

WebUI.setText(findTestObject('Page_Register/textfield_Name_username'), '')

String randomString = UUID.randomUUID().toString().replaceAll('-', '').substring(0, 10)

def email_code = ('cks' + randomString) + '@gmail.com'

WebUI.setText(findTestObject('Page_Register/textfield_Email_useremail'), email_code)

WebUI.setText(findTestObject('Page_Register/textfield_Password_userpassword'), '123456')

WebUI.click(findTestObject('Page_Register/btn_daftar'))

WebUI.verifyElementClickable(findTestObject('Page_Register/btn_daftar'))

WebUI.closeBrowser()

