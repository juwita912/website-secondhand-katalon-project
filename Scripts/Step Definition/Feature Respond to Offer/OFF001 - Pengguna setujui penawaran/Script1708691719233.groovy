import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://secondhand.binaracademy.org/')

WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/Page_Respond to Offer/input_Email_useremail'), 'imanjuwita@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Respond to Offer/input_Password_userpassword'), 'OGLiSTAGMFx0LAJ/xjX7tg==')

WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/input_Password_commit'))

WebUI.click(findTestObject('Page_Respond to Offer/i_New alerts_bell icon'))

WebUI.click(findTestObject('Page_Respond to Offer/a_Lihat semua notifikasi'))

WebUI.click(findTestObject('Page_Respond to Offer/Page_New Price 2/div_hapeRp 1.000.000Ditawar Rp 800.000'))

WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/label_Terima'))

WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/a_Hubungi di WA'))

WebUI.switchToWindowTitle('Share on WhatsApp')

WebUI.verifyElementVisible(findTestObject('Page_Respond to Offer/Page_Share on WhatsApp/img_wa'))

WebUI.closeBrowser()

