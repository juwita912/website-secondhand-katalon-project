import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.click(findTestObject('Page_User Login/btn_untuk masukkan login'))

WebUI.verifyElementVisible(findTestObject('Page_User Login/label_Masuk'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Page_User Login/textfield_username'))

WebUI.verifyElementVisible(findTestObject('Page_User Login/textfield_password'))

WebUI.verifyElementVisible(findTestObject('Page_User Login/btn_Masuk'))

WebUI.setText(findTestObject('Page_User Login/textfield_username'), 'overlord.njoo@gmail.com')

WebUI.setEncryptedText(findTestObject('Page_User Login/textfield_password'), 'JWDOtPzUpHberWoIJ37UJA==')

WebUI.click(findTestObject('Page_User Login/btn_Masuk'))

WebUI.click(findTestObject('Page_Edit_Product/btn_list_barang'))

WebUI.click(findTestObject('Page_Edit_Product/div_testing      Hobi      Rp 100'))

WebUI.click(findTestObject('Page_Edit_Product/btn_Edit'))

WebUI.setText(findTestObject('Page_Edit_Product/textfield_nama_produk'), '!@#')

WebUI.setText(findTestObject('Page_Edit_Product/textfield_harga_produk'), '1000000')

WebUI.selectOptionByValue(findTestObject('Page_Edit_Product/select_Pilih KategoriHobiKendaraanBajuElekt_20972a'), '2', true)

WebUI.setText(findTestObject('Page_Edit_Product/textfield_deskripsi'), 'test deskripsi katalon')

WebUI.scrollToElement(findTestObject('Page_Edit_Product/Images_Edit Product'), 0)

WebUI.click(findTestObject('Page_Edit_Product/btn_Terbitkan'))

WebUI.verifyElementClickable(findTestObject('Page_Edit_Product/btn_Edit'))

WebUI.closeBrowser()

